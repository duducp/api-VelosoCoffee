/**
 * Module dependencies.
 */
var express = require('express'),
    i18n = require("i18n"),
    compression = require('compression'),
    bodyParser = require('body-parser'),
    logger = require('morgan'),
    errorHandler = require('errorhandler'),
    dotenv = require('dotenv'),
    mongoose = require('mongoose'),
    mongooseIntl = require('mongoose-intl'),
    cors = require('cors'),
    path = require('path'),
    swaggerJSDoc = require('swagger-jsdoc');

/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 */
dotenv.load({path: '.env'});

/**
 * Create Express server.
 */
const app = express();
const server = require("http").Server(app);

/**
 * Express, cors, logger, compression configuration.
 */
app.set('port', process.env.PORT || 3000);
app.use(compression());
app.use(logger('dev'));
app.use(cors({
    origin: '*',
    allowedHeaders: ['Content-Type', 'Authorization', 'X-Requested-With', 'Accept', 'Origin']
}));
app.use(errorHandler());
app.use(i18n.init);

/**
 * Init body and cookie inside req
 * */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

/**
 * Init translator API
 * */
i18n.configure({
    locales:['en', 'pt'],
    defaultLocale: 'pt',
    queryParameter: 'lang', // query parameter to switch locale (ie. /home?lang=ch) - defaults to NULL
    directory: __dirname + '/locales',
    autoReload: true,
    register: global
});

/**
 * Connect to MongoDB.
 */
mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true });
mongoose.connection.on('error', function () {
    console.error('Falha ao conectar com o Banco de Dados');
    process.exit(1);
});

/**
 * Init translator API MongoDB
 * */
mongoose.plugin(mongooseIntl, {
    languages: ['en', 'pt'],
    defaultLanguage: 'pt'
});

/**
 * Load app modules and routes
 */
require('./src/app/routes/api')(app);

/**
 * Configuration Swagger
 */
let swaggerDefinition = {
  info: {
    title: 'Api Service KeepsCoffe',
    version: '1.0.0'
  },
  host: 'localhost:9000',
  basePath: '/api/v1'
}

let swaggerSpec = swaggerJSDoc({
    swaggerDefinition: swaggerDefinition,
    apis: ['./src/app/routes/*.js']
})

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'jade')
app.use(express.static(path.join(__dirname, 'swagger')))

app.get('/swagger.json', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  res.send(swaggerSpec)
})


/**
 * Start Express server.
 */
server.listen(app.get('port'), function () {
    console.log('Servidor rodando na porta ' + app.get('port') + ' em modo ' + app.get('env'));
});

module.exports = app;