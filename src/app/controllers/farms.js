'use strict';
var moment = require('moment'),
    mongoose = require('mongoose'),
    FarmModel = require('../models/farms');

exports.get = function (req, res, next) {
    FarmModel.setDefaultLanguage(req.query.lang);

    var obj = {};
    if(req.query.key == "_id")
        obj[req.query.key] = mongoose.Types.ObjectId(req.query.text);
    else
        obj[req.query.key] = req.query.text;

    FarmModel.find(obj).exec(function (err, farm) {
        if (err)
            return res.status(500).send({message: __('SEARCH_ERROR'), error: err});

        if (!farm || !farm.length)
            return res.status(422).send({message: __('SEARCH_NOT_FOUND')});

        //farm[0].setLanguage(lang)
        return res.status(200).json(farm);
    });
};

exports.create = function (req, res, next) {
    FarmModel.create(req.body, function (err, farm) {
        if (err)
            return res.status(500).send({message: __('CREATE_ERROR'), error: err});

        res.status(201).send({
            message: __('CREATE_SUCCESS'),
            result: farm
        });
    });
};

exports.delete = function (req, res, next) {
    var obj = {};
    obj["_id"] = mongoose.Types.ObjectId(req.params.id);

    FarmModel.findOne(obj, function (err, found) {
        if (err)
            return res.status(500).send({message: __('SEARCH_ERROR'), error: err});
        
        if (!found)
            return res.status(422).send({message: __('SEARCH_NOT_FOUND')});

        FarmModel.deleteOne(obj, function (err, deleted) {
            if (err)
                return res.status(500).send({message: __('DELETE_ERROR'), error: err});

            return res.status(200).send({message: __('DELETE_SUCCESS')});
        });
    });
};