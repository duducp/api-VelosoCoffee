'use strict';
var moment = require('moment'),
    SampleModel = require('../models/coffee-samples');

exports.get = function (req, res, next) {
    var lang = req.query.lang;

    var obj = {};
    if(req.query.key == "_id")
        obj[req.query.key] = mongoose.Types.ObjectId(req.query.text);
    else
        obj[req.query.key] = req.query.text;

    var query = SampleModel.aggregate([
        {$match: obj},

        {$lookup: {
            from: "farms",
            localField: "farm",
            foreignField: "_id",
            as: "farm"
        } },
        {$unwind: {path: "$farm", preserveNullAndEmptyArrays: true}},

        {$lookup: {
            from: "products",
            localField: "product",
            foreignField: "_id",
            as: "product"
        } },
        {$unwind: {path: "$product", preserveNullAndEmptyArrays: true}},

        {
            $group: {
                _id: "$_id",
                code: {$first: "$code"},
                description: {$first: "$description."+lang},
                date_planting: {$first: "$date_planting"},
                date_harvest: {$first: "$date_harvest"},
                text_marker: {$first: "$text_marker."+lang},
                map_center_coordinates: {$first: "$map_center_coordinates"},
                map_polygon_coordinates: {$first: "$map_polygon_coordinates"},
                gallery: {$first: "$gallery"},
                createdAt: {$first: "$createdAt"},
                updatedAt: {$first: "$updatedAt"},
                farm: {
                    $addToSet: {
                        _id: "$farm._id",
                        name: "$farm.name."+lang,
                        city: "$farm.city",
                        state: "$farm.state",
                        region: "$farm.region",
                        text_marker: "$farm.text_marker."+lang,
                        map_center_coordinates: "$farm.map_center_coordinates",
                        map_polygon_coordinates: "$farm.map_polygon_coordinates"
                    }
                },
                product: {
                    $addToSet: {
                        _id: "$product._id",
                        name: "$product.name",
                        grain_type: "$product.grain_type."+lang,
                        logo: "$product.logo"
                    }
                }
            }
        }
    ]);

    query.exec(function (err, sample) {
        if (err)
            return res.status(500).send({message: __('SEARCH_ERROR'), error: err});

        if (!sample || !sample.length)
            return res.status(422).send({message: __('SEARCH_NOT_FOUND')});

        return res.status(200).json(sample);
    });
};

exports.create = function (req, res, next) {
    SampleModel.create(req.body, function (err, sample) {
        if (err)
            return res.status(500).send({message: __('CREATE_ERROR'), error: err});

        res.status(201).send({
            message: __('CREATE_SUCCESS'),
            result: sample
        });
    });
};

exports.delete = function (req, res, next) {
    SampleModel.findOne({code: req.params.code}, function (err, found) {        
        if (err)
            return res.status(500).send({message: __('SEARCH_ERROR'), error: err});
        
        if (!found)
            return res.status(422).send({message: __('SEARCH_NOT_FOUND')});

        SampleModel.deleteOne({code: req.params.code}, function (err, deleted) {
            if (err)
                return res.status(500).send({message: __('DELETE_ERROR'), error: err});

            return res.status(200).send({message: __('DELETE_SUCCESS')});
        });
    });
};

exports.update = function (req, res, next) {
    var optionsObj = {
        new: true,
        upsert: true
    };

    Project.findByIdAndUpdate(req.params.id_project, {$set: req.body}, optionsObj, function (err, updateProject) {
        if (err)
            return res.status(500).send({message: 'Erro ao atualizar projeto', error: err});

        if (updateProject.id_user.toString() !== req.user._id.toString() && req.user.role !== 'admin') {
            res.status(401).send({message: 'Você não está autorizado a modificar este projeto'});
            return next('Não autorizado');
        }

        res.status(200).send({
            message: 'Projeto atualizado com sucesso',
            project: updateProject
        });
    });
};