'use strict';
var moment = require('moment'),
    mongoose = require('mongoose'),
    ProductModel = require('../models/products');

exports.get = function (req, res, next) {
    ProductModel.setDefaultLanguage(req.query.lang);

    var obj = {};
    if(req.query.key == "_id")
        obj[req.query.key] = mongoose.Types.ObjectId(req.query.text);
    else
        obj[req.query.key] = req.query.text;

    ProductModel.find(obj).exec(function (err, resp) {
        if (err)
            return res.status(500).send({message: __('SEARCH_ERROR'), error: err});

        if (!resp || !resp.length)
            return res.status(422).send({message: __('SEARCH_NOT_FOUND')});

        //resp[0].setLanguage(lang)
        return res.status(200).json(resp);
    });
};

exports.create = function (req, res, next) {
    ProductModel.create(req.body, function (err, resp) {
        if (err)
            return res.status(500).send({message: __('CREATE_ERROR'), error: err});

        res.status(201).send({
            message: __('CREATE_SUCCESS'),
            result: resp
        });
    });
};

exports.delete = function (req, res, next) {
    var obj = {};
    obj["_id"] = mongoose.Types.ObjectId(req.params.id);

    ProductModel.findOne(obj, function (err, found) {
        if (err)
            return res.status(500).send({message: __('SEARCH_ERROR'), error: err});
        
        if (!found)
            return res.status(422).send({message: __('SEARCH_NOT_FOUND')});

        ProductModel.deleteOne(obj, function (err, deleted) {
            if (err)
                return res.status(500).send({message: __('DELETE_ERROR'), error: err});

            return res.status(200).send({message: __('DELETE_SUCCESS')});
        });
    });
};