'use strict';
var moment = require('moment'),
    mongoose = require('mongoose'),
    RatingSamplesModel = require('../models/rating-samples');

exports.get = function (req, res, next) {
    RatingSamplesModel.setDefaultLanguage(req.query.lang);

    var obj = {};
    if(req.query.key == "_id")
        obj[req.query.key] = mongoose.Types.ObjectId(req.query.text);
    else
        obj[req.query.key] = req.query.text;

    RatingSamplesModel.find(obj).exec(function (err, resp) {
        if (err)
            return res.status(500).send({message: __('SEARCH_ERROR'), error: err});

        if (!resp || !resp.length)
            return res.status(422).send({message: __('SEARCH_NOT_FOUND')});

        return res.status(200).json(resp);
    });
};

exports.create = function (req, res, next) {
    RatingSamplesModel.create(req.body, function (err, resp) {
        if (err)
            return res.status(500).send({message: __('CREATE_ERROR'), error: err});

        res.status(201).send({
            message: __('CREATE_SUCCESS'),
            result: resp
        });
    });
};