var mongoose = require('mongoose');

var CoffeeSamplesSchema = new mongoose.Schema({
    code: {
        type: String,
        required: true,
        index: {unique: true},
    },
    description: {
        type: String,
        required: [true, 'A amostra deve ter uma descrição'],
        intl: true
    },
    product: {
        type: mongoose.Schema.ObjectId,
        required: [true, 'Informe o produto']
    },
    farm: {
        type: mongoose.Schema.ObjectId,
        required: [true, 'Informe a fazenda de plantio'],
    },
    date_planting: {
        type: Date,
        required: [true, 'A data de plantio deve ser informada']
    },
    date_harvest: {
        type: Date
    },
    text_marker: {
        type: String,
        required: [true, 'Você deve informar um texto para a marca no Mapa'],
        intl: true
    },
    map_center_coordinates: {
        lat: {
            type: Number,
            required: [true, 'Informe a latitude para a centralização do mapa']
        },
        lng: {
            type: Number,
            required: [true, 'Informe a longitude para a centralização do mapa']
        }
    },
    map_polygon_coordinates: [{
        lat: {
            type: Number,
            required: [true, 'Informe a latitude do polígono']
        },
        lng: {
            type: Number,
            required: [true, 'Informe a longitude do polígono']
        }
    }],
    gallery: [{
        imageUrl: {
            type: String
        },
        title: {
            type: String
        }
    }],
}, {
    toJSON: {
        virtuals: true,
    },
    timestamps: true
});

module.exports = mongoose.model('CoffeSamples', CoffeeSamplesSchema);
