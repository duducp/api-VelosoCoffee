var mongoose = require('mongoose');

var RatingSamplesSchema = new mongoose.Schema({
    acidity: {
        type: Number,
        required: true
    },
    sweetness: {
        type: String,
        required: true
    },
    bitterness: {
        type: String,
        required: true
    },
    aroma: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    },
    code: {
        type: String,
        required: true
    },
    lang: {
        type: String,
        required: true
    },
    uuid: {
        type: String,
        required: false
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('RatingSamples', RatingSamplesSchema);