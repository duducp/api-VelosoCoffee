var mongoose = require('mongoose');

var ProductsSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Informe o nome do produto'],
        unique: true,
    },
    grain_type: {
        type: String,
        required: [true, 'Informe o tipo do café'],
        intl: true
    },
    logo: {
        type: String,
        required: [true, 'Informe o caminho da logo do produto']
    }
}, {
    toJSON: {
        virtuals: true,
    },
    timestamps: true
});

module.exports = mongoose.model('Products', ProductsSchema);