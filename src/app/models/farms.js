var mongoose = require('mongoose');

var FarmsSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Informe o nome do local'],
        intl: true
    },
    city: {
        type: String,
        required: [true, 'Informe a cidade de localização']
    },
    state: {
        type: String,
        required: [true, 'Informe o estado']
    },
    region: {
        type: String,
        required: [true, 'Informe a região']
    },
    text_marker: {
        type: String,
        required: [false, 'Você deve informar um texto para a marca no Mapa'],
        intl: true
    },
    map_center_coordinates: {
        lat: {
            type: Number,
            required: [false, 'Informe a latitude para a centralização do mapa']
        },
        lng: {
            type: Number,
            required: [false, 'Informe a longitude para a centralização do mapa']
        }
    },
    map_polygon_coordinates: [{
        lat: {
            type: Number,
            required: [false, 'Informe a latitude do polígono']
        },
        lng: {
            type: Number,
            required: [false, 'Informe a longitude do polígono']
        }
    }]
}, {
    toJSON: {
        virtuals: true,
    },
    timestamps: true
});

module.exports = mongoose.model('Farms', FarmsSchema);