/**
 * @swagger
 * definitions:
 *   register:
 *     properties:
 *       name:
 *         type: string
 *       email:
 *         type: string
 *       password:
 *         type: string
 *       department:
 *         type: string
 *       role:
 *         type: string
 *   login:
 *     properties:
 *       email:
 *         type: string
 *       password:
 *         type: string
 *   product:
 *     properties:
 *       name:
 *         type: string
 *       grain_type:
 *         type: object
 *         properties:
 *           en:
 *             type: string
 *           pt:
 *             type: string
 *       logo:
 *         type: string
 *   sample:
 *     properties:
 *       code:
 *         type: string
 *       product:
 *         type: string
 *       farm:
 *         type: string
 *       description:
 *         type: object
 *         properties:
 *           en:
 *             type: string
 *           pt:
 *             type: string
 *       text_marker:
 *         type: object
 *         properties:
 *           en:
 *             type: string
 *           pt:
 *             type: string
 *       date_planting:
 *         type: string
 *       date_harvest:
 *         type: string
 *       map_center_coordinates:
 *         type: object
 *         properties:
 *           lat:
 *             type: string
 *           lng:
 *             type: string
 *       map_polygon_coordinates:
 *         type: array
 *         items:
 *           type: object
 *           properties:
 *             lat:
 *               type: string
 *             lng:
 *               type: string
 *   ratingsample:
 *     properties:
 *       acidity:
 *         type: string
 *       code:
 *         type: string
 *       sweetness:
 *         type: object
 *         properties:
 *           en:
 *             type: string
 *           pt:
 *             type: string
 *       bitterness:
 *         type: object
 *         properties:
 *           en:
 *             type: string
 *           pt:
 *             type: string
 *       aroma:
 *         type: object
 *         properties:
 *           en:
 *             type: string
 *           pt:
 *             type: string
 *       body:
 *         type: object
 *         properties:
 *           en:
 *             type: string
 *           pt:
 *             type: string
 *   farm:
 *     properties:
 *       name:
 *         type: object
 *         properties:
 *           en:
 *             type: string
 *           pt:
 *             type: string
 *       city:
 *         type: string
 *       state:
 *         type: string
 *       region:
 *         type: string
 *       text_marker:
 *         type: string
*/
