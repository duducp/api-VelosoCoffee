'use strict';
const express = require('express'),
    passportService = require('../../config/passport'),
    passport = require('passport');

const AuthenticationController = require('../controllers/authentication'),
    SampleCoffeeController = require('../controllers/coffee-samples'),
    FarmController = require('../controllers/farms'),
    ProductController = require('../controllers/products'),
    RatingSampleController = require('../controllers/rating-samples');

const requireAuth = passport.authenticate('jwt', {session: false}),
    requireLogin = passport.authenticate('local', {session: false});

module.exports = function (app) {
    require('./swaggerDefinitions.js')

    const versionApi = express.Router(),
        apiRoutes = express.Router(),
        authRoutes = express.Router(),
        sampleRoutes = express.Router(),
        farmRoutes = express.Router(),
        productRoutes = express.Router(),
        ratingSampleRoutes = express.Router();

    // Set up routes
    app.use('/api', versionApi);

    // Api V1
    versionApi.use('/v1',
        /**
         * @swagger
         * /:
         *   get:
         *     tags:
         *       - Hello World
         *     description: Hello World
         *     produces:
         *       - application/json
         *     responses:
         *       200:
         *         description: OK!
         *
         */
        apiRoutes.get('/', function (req, res) {
            res.writeHead(200, {'Content-Type': 'text/plain'});
            res.end('Hello there, world!\n');
        }),

        apiRoutes.use('/sample',
            /**
             * @swagger
             * /sample:
             *   get:
             *     tags:
             *       - Amostras de Café
             *     description: Retorna as amostras cadastradas
             *     produces:
             *       - application/json
             *     parameters:
             *       - name: lang
             *         description: Código da linguagem para retorno do conteúdo (pt, en)
             *         in: path
             *         type: string
             *         default: pt
             *       - name: key
             *         description: Chave para fazer o filtro. Exemplo city
             *         in: path
             *         type: string
             *       - name: text
             *         description: Texto para fazer o filtro
             *         in: path
             *         type: string
             *     responses:
             *       200:
             *         description: OK!
             *       500:
             *         description: Internal Server Error!
             *
            */
            sampleRoutes.get('/', SampleCoffeeController.get),

            /**
             * @swagger
             * /sample:
             *   post:
             *     tags:
             *       - Amostras de Café
             *     description: Cadastra uma amostra de café
             *     produces:
             *       - application/json
             *     parameters:
             *       - name: sample
             *         description: Dados para cadastro
             *         in: body
             *         required: true
             *         schema:
             *           $ref: '#/definitions/sample'
             *     responses:
             *       200:
             *         description: OK!
             *       422:
             *         description: Dados duplicados
            */
            sampleRoutes.post('/', SampleCoffeeController.create),

            /**
             * @swagger
             * /sample/{id}:
             *   delete:
             *     tags:
             *       - Amostras de Café
             *     description: Exclui uma amostra
             *     produces:
             *       - application/json
             *     parameters:
             *       - name: id
             *         description: Id da amostra
             *         in: path
             *         required: true
             *         type: string
             *     responses:
             *       200:
             *         description: OK!
             *       500:
             *         description: Internal Server Error!
             *
            */
            sampleRoutes.delete('/:code', SampleCoffeeController.delete)
        ),

        apiRoutes.use('/farm',

            /**
             * @swagger
             * /farm:
             *   get:
             *     tags:
             *       - Fazendas
             *     description: Retorna as fazendas cadastradas
             *     produces:
             *       - application/json
             *     parameters:
             *       - name: lang
             *         description: Código da linguagem para retorno do conteúdo (pt, en)
             *         in: path
             *         type: string
             *         default: pt
             *       - name: key
             *         description: Chave para fazer o filtro. Exemplo city
             *         in: path
             *         type: string
             *       - name: text
             *         description: Texto para fazer o filtro
             *         in: path
             *         type: string
             *     responses:
             *       200:
             *         description: OK!
             *       500:
             *         description: Internal Server Error!
             *
            */
            farmRoutes.get('/', FarmController.get),

            /**
             * @swagger
             * /farm:
             *   post:
             *     tags:
             *       - Fazendas
             *     description: Cadastra uma fazenda
             *     produces:
             *       - application/json
             *     parameters:
             *       - name: farm
             *         description: Dados para cadastro
             *         in: body
             *         required: true
             *         schema:
             *           $ref: '#/definitions/farm'
             *     responses:
             *       200:
             *         description: OK!
             *       422:
             *         description: Dados duplicados
            */
            farmRoutes.post('/', FarmController.create),

            /**
             * @swagger
             * /farm/{id}:
             *   delete:
             *     tags:
             *       - Fazendas
             *     description: Exclui uma fazenda
             *     produces:
             *       - application/json
             *     parameters:
             *       - name: id
             *         description: Id da fazenda
             *         in: path
             *         required: true
             *         type: string
             *     responses:
             *       200:
             *         description: OK!
             *       500:
             *         description: Internal Server Error!
             *
            */
            farmRoutes.delete('/:id', FarmController.delete)
        ),

        apiRoutes.use('/product',

             /**
             * @swagger
             * /product:
             *   get:
             *     tags:
             *       - Produtos
             *     description: Retorna os produtos cadastradas
             *     produces:
             *       - application/json
             *     parameters:
             *       - name: lang
             *         description: Código da linguagem para retorno do conteúdo (pt, en)
             *         in: path
             *         type: string
             *         default: pt
             *       - name: key
             *         description: Chave para fazer o filtro. Exemplo city
             *         in: path
             *         type: string
             *       - name: text
             *         description: Texto para fazer o filtro
             *         in: path
             *         type: string
             *     responses:
             *       200:
             *         description: OK!
             *       500:
             *         description: Internal Server Error!
             *
            */
            productRoutes.get('/', ProductController.get),

            /**
             * @swagger
             * /product:
             *   post:
             *     tags:
             *       - Produtos
             *     description: Cadastra um produto
             *     produces:
             *       - application/json
             *     parameters:
             *       - name: product
             *         description: Dados para cadastro
             *         in: body
             *         required: true
             *         schema:
             *           $ref: '#/definitions/product'
             *     responses:
             *       200:
             *         description: OK!
             *       422:
             *         description: Dados duplicados
            */
            productRoutes.post('/', ProductController.create),

            /**
             * @swagger
             * /product/{id}:
             *   delete:
             *     tags:
             *       - Produtos
             *     description: Exclui o produto
             *     produces:
             *       - application/json
             *     parameters:
             *       - name: id
             *         description: Id do produto
             *         in: path
             *         required: true
             *         type: string
             *     responses:
             *       200:
             *         description: OK!
             *       500:
             *         description: Internal Server Error!
             *
            */
            productRoutes.delete('/:id', ProductController.delete)
        ),

        apiRoutes.use('/rating-sample',
            /**
             * @swagger
             * /rating-sample:
             *   get:
             *     tags:
             *       - Avaliação de Amostras de Café
             *     description: Retorna as avaliações cadastradas
             *     produces:
             *       - application/json
             *     parameters:
             *       - name: lang
             *         description: Código da linguagem para retorno do conteúdo (pt, en)
             *         in: path
             *         type: string
             *         default: pt
             *       - name: key
             *         description: Chave para fazer o filtro. Exemplo city
             *         in: path
             *         type: string
             *       - name: text
             *         description: Texto para fazer o filtro
             *         in: path
             *         type: string
             *     responses:
             *       200:
             *         description: OK!
             *       500:
             *         description: Internal Server Error!
             *
            */
            ratingSampleRoutes.get('/', RatingSampleController.get),

            /**
             * @swagger
             * /rating-sample:
             *   post:
             *     tags:
             *       - Avaliação de Amostras de Café
             *     description: Cadastra uma nova avaliação de amostra de café
             *     produces:
             *       - application/json
             *     parameters:
             *       - name: rating-sample
             *         description: Dados para cadastro
             *         in: body
             *         required: true
             *         schema:
             *           $ref: '#/definitions/ratingsample'
             *     responses:
             *       200:
             *         description: OK!
             *       422:
             *         description: Dados duplicados
            */
            ratingSampleRoutes.post('/', RatingSampleController.create)
        ),

        // Auth Routes
        apiRoutes.use('/auth',
            /**
             * @swagger
             * /auth/register:
             *   post:
             *     tags:
             *       - Autenticação
             *     description: Realiza o cadastro do usuário no sistema
             *     produces:
             *       - application/json
             *     parameters:
             *       - name: register
             *         description: Dados para cadastro
             *         in: body
             *         required: true
             *         schema:
             *           $ref: '#/definitions/register'
             *     responses:
             *       200:
             *         description: OK!
             *       422:
             *         description: Dados duplicados
            */
            authRoutes.post('/register', AuthenticationController.register),

            /**
             * @swagger
             * /auth/login:
             *   post:
             *     tags:
             *       - Autenticação
             *     description: Realiza login no sistema
             *     produces:
             *       - application/json
             *     parameters:
             *       - name: login
             *         description: Dados para realizar login
             *         in: body
             *         required: true
             *         schema:
             *           $ref: '#/definitions/login'
             *     responses:
             *       200:
             *         description: OK!
             *       401:
             *         description: Usuário sem autorização
            */
            authRoutes.post('/login', requireLogin, AuthenticationController.login)
        )
    );
};