'use strict';
const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const LocalStrategy = require('passport-local').Strategy;
const User = require('../app/models/user');

var localOptions = {
    usernameField: 'email'
};

var localLogin = new LocalStrategy(localOptions, function (email, password, done) {
    User.findOne({
        email: email
    }, function (err, user) {
        if (err)
            return done(err);

        if (!user) {
            var content = {
                message: 'Email informado está incorreto.'
            };
            return done(null, false, content);
        }

        user.comparePassword(password, function (err, isMatch) {
            if (err) {
                return done(err);
            }

            var content = {
                message: 'Senha informada está incorreta.'
            };

            if (!isMatch) {
                return done(null, false, content);
            }

            return done(null, user);
        });
    })
});

//https://www.npmjs.com/package/passport-jwt#extracting-the-jwt-from-the-request
var jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.TOKEN_SECRET
};

var jwtLogin = new JwtStrategy(jwtOptions, function (payload, done) {
    User.findById(payload._id, function (err, user) {
        if (err) return done(new Error("User not found"), false);

        if (user) {
            return done(null, user); //envia os dados do usuário
        } else {
            return done(null, false);
        }
    });
});

passport.use(jwtLogin);
passport.use(localLogin);